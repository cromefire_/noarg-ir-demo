package com.example.demo

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Model(
    @Id
    @GeneratedValue
    val id: Long = -1,
    @Column(nullable = false)
    val string: String
)
